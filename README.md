# D0018E Group 7 Site: Cykel och Bad

## User stories

### Customer (Not logged in)

A not logged in customer can:

1. Browse products
2. Search products
3. Browse packet deals
4. Store products a shopping cart between sessions.

### Customer (Logged in)

A logged in customer can:

1. Browse products
2. Search products
3. Browse packet deals
4. Store multiple shopping carts
5. Purchase Duck-coins
6. Place orders with products up to `rare`-tier

### Premim Customer

A customer subscriped to the premium service can:

1. Browse products
2. Search products
3. Browse packet deals
4. Store multiple shopping carts
5. Purchase Duck-coins
6. Place orders with products up to `divine`-tier

### Product Manager

A product manager can:

1. Browse products
2. Search products
3. Add / Edit / Remove products
4. Browse packet deals
5. Add / Edit / Remove packet deals

### Review Manager

A review manager can:
1. Browse products
2. Search products
3. Browse packet deals
4. Browse users
5. Search users
6. Moderate reviews

### Admin

An admin can:

1. Browse products
2. Search products
3. Add / Edit / Remove products
4. Browse packet deals
5. Add / Edit / Remove packet deals
6. Browse users
7. Search users
8. Moderate reviews
9. Moderate users

## System Architecture

The following information is for setup on a Linux-based System. For information on how to do the setup on other operating systems please refer to [here](https://www.google.com/)

### Server environment
#### Repo setup
##### Prerequisites:

1. [Git](https://git-scm.com/ "Git Homepage")

##### Setup:

```bash
git clone git@gitlab.com:edvker-7/cykelochbad.git cykelochbad
```

#### Python evironment

##### Prerequisites:

1. [Python3.4+](https://www.python.org/ "Python Homepage")

##### Setup:

```bash
cd path/to/cykelochbad
python3 -m venv venv
source venv/bin/activate
pip install -r prerequisites.txt
exit
```

#### MySQL Server and Schema setup

##### Prerequisites

1. [MySQL-server 5.7+](https://www.mysql.com/ "MySQL Homepage")

##### Config file setup

Add `bind-address = 0.0.0.0` below `bind-address = 127.0.0.1` in `/etc/mysql/mysql.conf.d/mysqld.cnf`

##### Secure Setup

Run ```mysql_secure_setup```

1. Set root password
2. Remove anonymous users? **Yes**
3. Disallow root login remotely? **Yes**
4. Remove test database and access to it? **Yes**
5. Reload privilege table now? **Yes**

##### Setup users

Run `sudo mysql` then enter the following queries into MySQL, replace username and password with your own:

```sql
USE mysql;
CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
CREATE USER 'username'@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'username'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'username'@'%';
FLUSH PRIVILEGES;
exit
```

Run `service restart mysql`

##### Setup schema

Run `mysql` then enter the following queries into MySQL:

```sql
source mysql/complete_schema_setup.sql
```

#### Run Flask Server

```bash
source venv/bin/activate
export FLASK_APP=/src/main.py
flask run
```

## Bibliography

### Python Flask Webserver

#### Python environment

`https://wiki.archlinux.org/index.php/Python/Virtual_environment`

#### Flask documentation:

`http://flask.palletsprojects.com/en/1.1.x/`

#### Jinja documentation:

`https://jinja.palletsprojects.com/en/2.10.x/`

#### Reason for Flask Blueprints

`https://stackoverflow.com/questions/40706854/splitting-a-flask-app-into-multiple-files/40706946`

### MySQL Server and Database

#### MySQL Server ubuntu configuration

`https://support.rackspace.com/how-to/installing-mysql-server-on-ubuntu/`

#### MySQL Server users / hosts configuration

`https://stackoverflow.com/questions/39281594/error-1698-28000-access-denied-for-user-rootlocalhost`

### Git

#### Git documentation

`https://docs.gitlab.com/`

#### Automatic Deployment (not yet implemented)

`Oscar Brink, Group 32`

### Other

#### README structure inspiration:

`Oscar Brink, Lisa Jonsson, Anton Johansson, Group 32`
