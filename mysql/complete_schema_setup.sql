-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema cykelochbad
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `cykelochbad` ;

-- -----------------------------------------------------
-- Schema cykelochbad
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cykelochbad` DEFAULT CHARACTER SET latin1 ;
USE `cykelochbad` ;

-- -----------------------------------------------------
-- Table `Role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Role` ;

CREATE TABLE IF NOT EXISTS `Role` (
  `RoleID` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `RoleName` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`RoleID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Person` ;

CREATE TABLE IF NOT EXISTS `Person` (
  `PersonID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(50) NOT NULL,
  `LastName` VARCHAR(50) NOT NULL,
  `Email` VARCHAR(50) NOT NULL,
  `RoleID` SMALLINT(5) UNSIGNED NOT NULL,
  `authentication_string` VARCHAR(255) NOT NULL,
  `Balance` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `Picture` VARCHAR(255) NULL DEFAULT NULL,
  `DateCreated` DATE NULL DEFAULT NULL,
  `DateTimeLastActive` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`PersonID`),
  UNIQUE INDEX `Email` (`Email` ASC) VISIBLE,
  INDEX `RoleID` (`RoleID` ASC) VISIBLE,
  CONSTRAINT `Person_ibfk_1`
    FOREIGN KEY (`RoleID`)
    REFERENCES `Role` (`RoleID`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `OrderData`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `OrderData` ;

CREATE TABLE IF NOT EXISTS `OrderData` (
  `OrderID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `PersonID` INT(10) UNSIGNED NOT NULL,
  `PurchaseDate` DATETIME NOT NULL,
  `OrderName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`OrderID`),
  INDEX `PersonID` (`PersonID` ASC) VISIBLE,
  CONSTRAINT `OrderData_ibfk_1`
    FOREIGN KEY (`PersonID`)
    REFERENCES `Person` (`PersonID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Rarity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Rarity` ;

CREATE TABLE IF NOT EXISTS `Rarity` (
  `RarityID` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `RarityName` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`RarityID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Product` ;

CREATE TABLE IF NOT EXISTS `Product` (
  `ProductID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ProductName` VARCHAR(255) NOT NULL,
  `RarityID` SMALLINT(5) UNSIGNED NOT NULL,
  `Image` VARCHAR(255) NULL DEFAULT NULL,
  `Price` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ProductID`),
  INDEX `RarityID` (`RarityID` ASC) VISIBLE,
  CONSTRAINT `Product_ibfk_1`
    FOREIGN KEY (`RarityID`)
    REFERENCES `Rarity` (`RarityID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `OrderItem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `OrderItem` ;

CREATE TABLE IF NOT EXISTS `OrderItem` (
  `OrderID` INT(10) UNSIGNED NOT NULL,
  `ProductID` INT(10) UNSIGNED NOT NULL,
  `Price` INT(10) UNSIGNED NOT NULL,
  `Amount` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`OrderID`, `ProductID`),
  INDEX `ProductID` (`ProductID` ASC) VISIBLE,
  CONSTRAINT `OrderItem_ibfk_1`
    FOREIGN KEY (`OrderID`)
    REFERENCES `OrderData` (`OrderID`),
  CONSTRAINT `OrderItem_ibfk_2`
    FOREIGN KEY (`ProductID`)
    REFERENCES `Product` (`ProductID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Packet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Packet` ;

CREATE TABLE IF NOT EXISTS `Packet` (
  `PacketID` INT(10) UNSIGNED NOT NULL,
  `RarityID` SMALLINT(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`PacketID`),
  INDEX `RarityID` (`RarityID` ASC) VISIBLE,
  CONSTRAINT `Packet_ibfk_1`
    FOREIGN KEY (`RarityID`)
    REFERENCES `Rarity` (`RarityID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `PacketGiven`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PacketGiven` ;

CREATE TABLE IF NOT EXISTS `PacketGiven` (
  `PacketID` INT(10) UNSIGNED NOT NULL,
  `ProductID` INT(10) UNSIGNED NOT NULL,
  `Amount` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`PacketID`, `ProductID`),
  INDEX `ProductID` (`ProductID` ASC) VISIBLE,
  CONSTRAINT `PacketGiven_ibfk_1`
    FOREIGN KEY (`PacketID`)
    REFERENCES `Packet` (`PacketID`),
  CONSTRAINT `PacketGiven_ibfk_2`
    FOREIGN KEY (`ProductID`)
    REFERENCES `Product` (`ProductID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `PacketNeeded`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `PacketNeeded` ;

CREATE TABLE IF NOT EXISTS `PacketNeeded` (
  `PacketID` INT(10) UNSIGNED NOT NULL,
  `ProductID` INT(10) UNSIGNED NOT NULL,
  `Amount` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`PacketID`, `ProductID`),
  INDEX `ProductID` (`ProductID` ASC) VISIBLE,
  CONSTRAINT `PacketNeeded_ibfk_1`
    FOREIGN KEY (`PacketID`)
    REFERENCES `Packet` (`PacketID`),
  CONSTRAINT `PacketNeeded_ibfk_2`
    FOREIGN KEY (`ProductID`)
    REFERENCES `Product` (`ProductID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ShoppingCart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShoppingCart` ;

CREATE TABLE IF NOT EXISTS `ShoppingCart` (
  `ShoppingCartID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `PersonID` INT(10) UNSIGNED NOT NULL,
  `ShoppingCartName` VARCHAR(100) NOT NULL DEFAULT 'Untitled',
  PRIMARY KEY (`ShoppingCartID`),
  INDEX `PersonID` (`PersonID` ASC) VISIBLE,
  CONSTRAINT `ShoppingCart_ibfk_1`
    FOREIGN KEY (`PersonID`)
    REFERENCES `Person` (`PersonID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ShoppingCartItem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ShoppingCartItem` ;

CREATE TABLE IF NOT EXISTS `ShoppingCartItem` (
  `ShoppingCartID` INT(10) UNSIGNED NOT NULL,
  `ProductID` INT(10) UNSIGNED NOT NULL,
  `Amount` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ShoppingCartID`, `ProductID`),
  INDEX `ProductID` (`ProductID` ASC) VISIBLE,
  CONSTRAINT `ShoppingCartItem_ibfk_1`
    FOREIGN KEY (`ShoppingCartID`)
    REFERENCES `ShoppingCart` (`ShoppingCartID`),
  CONSTRAINT `ShoppingCartItem_ibfk_2`
    FOREIGN KEY (`ProductID`)
    REFERENCES `Product` (`ProductID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Stats`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Stats` ;

CREATE TABLE IF NOT EXISTS `Stats` (
  `ProductID` INT(10) UNSIGNED NOT NULL,
  `Strength` SMALLINT(6) NOT NULL DEFAULT '0',
  `Dexterity` SMALLINT(6) NOT NULL DEFAULT '0',
  `Intelligence` SMALLINT(6) NOT NULL DEFAULT '0',
  `Constitution` SMALLINT(6) NOT NULL DEFAULT '0',
  `Speed` SMALLINT(6) NOT NULL DEFAULT '0',
  `Perception` SMALLINT(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProductID`),
  CONSTRAINT `Stats_ibfk_1`
    FOREIGN KEY (`ProductID`)
    REFERENCES `Product` (`ProductID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `Review`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Review` ;

CREATE TABLE IF NOT EXISTS `Review` (
  `ReviewID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `PersonID` INT UNSIGNED NOT NULL,
  `ProductID` INT UNSIGNED NOT NULL,
  `Rating` TINYINT NOT NULL,
  `Text` VARCHAR(500) CHARACTER SET 'latin1' NULL,
  PRIMARY KEY (`ReviewID`),
  INDEX `ProductID_idx` (`ProductID` ASC) VISIBLE,
  INDEX `PersonID_idx` (`PersonID` ASC) VISIBLE,
  UNIQUE INDEX `OnePerProduct` (`PersonID` ASC, `ProductID` ASC) VISIBLE,
  INDEX `Rating` (`Rating` DESC) VISIBLE,
  CONSTRAINT `ProductID`
    FOREIGN KEY (`ProductID`)
    REFERENCES `Product` (`ProductID`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `PersonID`
    FOREIGN KEY (`PersonID`)
    REFERENCES `Person` (`PersonID`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `Role`
-- -----------------------------------------------------
START TRANSACTION;
USE `cykelochbad`;
INSERT INTO `Role` (`RoleID`, `RoleName`) VALUES (DEFAULT, 'Admin');
INSERT INTO `Role` (`RoleID`, `RoleName`) VALUES (DEFAULT, 'Review Manager');
INSERT INTO `Role` (`RoleID`, `RoleName`) VALUES (DEFAULT, 'Product Manager');
INSERT INTO `Role` (`RoleID`, `RoleName`) VALUES (DEFAULT, 'Premium User');
INSERT INTO `Role` (`RoleID`, `RoleName`) VALUES (DEFAULT, 'User');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Rarity`
-- -----------------------------------------------------
START TRANSACTION;
USE `cykelochbad`;
INSERT INTO `Rarity` (`RarityID`, `RarityName`) VALUES (DEFAULT, 'Common');
INSERT INTO `Rarity` (`RarityID`, `RarityName`) VALUES (DEFAULT, 'Rare');
INSERT INTO `Rarity` (`RarityID`, `RarityName`) VALUES (DEFAULT, 'Epic');
INSERT INTO `Rarity` (`RarityID`, `RarityName`) VALUES (DEFAULT, 'Legendary');
INSERT INTO `Rarity` (`RarityID`, `RarityName`) VALUES (DEFAULT, 'Divine');

COMMIT;

