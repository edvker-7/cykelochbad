CREATE TABLE cykelochbad.OrderItem (
    OrderID INT UNSIGNED NOT NULL,
    ProductID INT UNSIGNED NOT NULL,
    Price INT UNSIGNED NOT NULL,
    Amount INT UNSIGNED NOT NULL,
    PRIMARY KEY (OrderID , ProductID),
    FOREIGN KEY (OrderID)
        REFERENCES cykelochbad.OrderData (OrderID),
    FOREIGN KEY (ProductID)
        REFERENCES cykelochbad.Product (ProductID)
);