CREATE TABLE cykelochbad.ShoppingCartItem (
    ShoppingCartID INT UNSIGNED NOT NULL,
    ProductID INT UNSIGNED NOT NULL,
    Amount INT UNSIGNED NOT NULL,
    PRIMARY KEY (ShoppingCartID , ProductID),
    FOREIGN KEY (ShoppingCartID)
        REFERENCES cykelochbad.ShoppingCart (ShoppingCartID),
    FOREIGN KEY (ProductID)
        REFERENCES cykelochbad.Product (ProductID)
);