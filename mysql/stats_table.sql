CREATE TABLE cykelochbad.Stats (
    ProductID INT UNSIGNED NOT NULL,
    Strength SMALLINT NOT NULL DEFAULT 0,
    Dexterity SMALLINT NOT NULL DEFAULT 0,
    Intelligence SMALLINT NOT NULL DEFAULT 0,
    Constitution SMALLINT NOT NULL DEFAULT 0,
    Speed SMALLINT NOT NULL DEFAULT 0,
    Perception SMALLINT NOT NULL DEFAULT 0,
    PRIMARY KEY (ProductID),
    FOREIGN KEY (ProductID)
        REFERENCES cykelochbad.Product (ProductID)
);