CREATE TABLE cykelochbad.Person (
    PersonID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    Email VARCHAR(50) NOT NULL UNIQUE,
    RoleID SMALLINT UNSIGNED NOT NULL,
    authentication_string VARCHAR(255) NOT NULL,
    Balance INT UNSIGNED NOT NULL DEFAULT 0,
    Picture VARCHAR(255),
    DateCreated DATE,
    DateTimeLastActive DATE,
    PRIMARY KEY (PersonID),
    FOREIGN KEY (RoleID)
        REFERENCES cykelochbad.Role (RoleID)
);
