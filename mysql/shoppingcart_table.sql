CREATE TABLE cykelochbad.ShoppingCart (
    ShoppingCartID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PersonID INT UNSIGNED NOT NULL,
    ShoppingCartName VARCHAR(100) NOT NULL DEFAULT 'Untitled',
    PRIMARY KEY (ShoppingCartID),
    FOREIGN KEY (PersonID)
        REFERENCES cykelochbad.Person (PersonID)
);