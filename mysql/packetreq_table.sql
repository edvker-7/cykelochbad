CREATE TABLE cykelochbad.PacketNeeded (
    PacketID INT UNSIGNED NOT NULL,
    ProductID INT UNSIGNED NOT NULL,
    Amount INT UNSIGNED NOT NULL,
    PRIMARY KEY (PacketID , ProductID),
    FOREIGN KEY (PacketID)
        REFERENCES cykelochbad.Packet (PacketID),
    FOREIGN KEY (ProductID)
        REFERENCES cykelochbad.Product (ProductID)
);