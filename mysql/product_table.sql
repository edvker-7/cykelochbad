CREATE TABLE cykelochbad.Product (
    ProductID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    ProductName VARCHAR(255) NOT NULL,
    RarityID SMALLINT UNSIGNED NOT NULL,
    Image VARCHAR(255),
    Price INT UNSIGNED NOT NULL,
    PRIMARY KEY (ProductID),
    FOREIGN KEY (RarityID)
        REFERENCES cykelochbad.Rarity (RarityID)
);