CREATE TABLE cykelochbad.Rarity (
    RarityID SMALLINT UNSIGNED NOT NULL,
    RarityName VARCHAR(20) NOT NULL,
    PRIMARY KEY (RarityID)
);