CREATE TABLE cykelochbad.OrderData (
    OrderID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    PersonID INT UNSIGNED NOT NULL,
    PurchaseDate DATETIME NOT NULL,
    OrderName VARCHAR(100) NOT NULL,
    PRIMARY KEY (OrderID),
    FOREIGN KEY (PersonID)
        REFERENCES cykelochbad.Person (PersonID)
);