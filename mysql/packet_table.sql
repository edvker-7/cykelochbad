CREATE TABLE cykelochbad.Packet (
    PacketID INT UNSIGNED NOT NULL,
    RarityID SMALLINT UNSIGNED NOT NULL,
    PRIMARY KEY (PacketID),
    FOREIGN KEY (RarityID)
        REFERENCES cykelochbad.Rarity (RarityID)
);