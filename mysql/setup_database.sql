source database_setup.sql;
source role_table.sql;
source rarity_table.sql;
source product_table.sql;
source stats_table.sql;
source packet_table.sql;
source packetreq_table.sql;
source packetres_table.sql;
source user_table.sql;
source orderdata_table.sql;
source orderitem_table.sql;
source shoppingcart_table.sql;
source shoppingcartitem_table.sql;
