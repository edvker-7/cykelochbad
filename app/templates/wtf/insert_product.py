{% extends 'signin.html' %}
{% block signin_form %}
  <form method="POST" action="">
    {{ form.csrf_token }}
    <div> {{ form.first_name.label }}: {{ form.first_name() }} </div>
    <div> {{ form.last_name.label }}: {{ form.last_name() }} </div>
    <div> {{ form.email.label }}: {{ form.email(class="") }} </div>
    <div> {{ form.password.label }}: {{ form.password(class="") }} </div>
    <div> {{ form.submit(class="") }} </div>
  </form>
{% endblock %}
