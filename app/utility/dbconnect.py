import mysql.connector

class DatabaseConnection:
    def __enter__(self):
        self.dbconn = mysql.connector.connect(option_files='app/utility/db.conf')
        return self.dbconn

    def __exit__(self, exc_type, exc_value, traceback):
        self.dbconn.close()
