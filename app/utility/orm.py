from app import DatabaseConnection
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
import sys
#========================== Insertions ============================

def insertUser(email,fname,lname,role, pwd):
    #hash pwd
    hashed_pwd = generate_password_hash(pwd)
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("INSERT INTO Person VALUES (DEFAULT,%s,%s,%s,%s,%s,0,NULL,%s,NULL)",
                    (fname,lname,email,role,hashed_pwd,datetime.now().strftime('%Y-%m-%d')))
            db.commit()
        except Exception as e:
            #handle Connection errors
            return e
        finally:
            cur.close()

def insertProduct(name,rarity,image,price):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("INSERT INTO Product VALUES (DEFAULT,%s,%s,%s,%s)",(name,rarity,image,price))
            db.commit()
            return cur.lastrowid
        except Exception as e:
            #handle Connection errors
            return e
        finally:
            cur.close()

def insertShoppingCart(uid, name):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cmd = f"INSERT INTO ShoppingCart(PersonID,ShoppingCartName) VALUES ({uid},'{name}');"
            print(cmd)
            cur.execute(cmd)
            db.commit()
        except:
            #handle Connection errors
            print("Unexpected error:", sys.exc_info()[0])
            return None
        finally:
            cur.close()

def insertReview(uid,pid, text, rating):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("INSERT INTO Review VALUES (DEFAULT,%s,%s,%s,%s)",(uid,pid,rating,text))
            db.commit()
            return cur.lastrowid
        except Exception as e:
            #handle Connection errors
            return e
        finally:
            cur.close()
#========================== Specific Requests ==========================
def getUserByEmail(email):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("SELECT * FROM person WHERE Email=%s", (email,))
            return cur.fetchone()
        except Exception as e:
            #handle Connection errors
            return e
        finally:
            cur.close()

def getUserById(uid):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("SELECT * FROM person WHERE PersonID=%s", (uid,))
            return cur.fetchone()
        except Exception as e:
            #handle Connection errors
            return e
        finally:
            cur.close()

def getRarities():
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("SELECT * FROM Rarity")
            return cur.fetchall()
        except Exception as e:
            return e
        finally:
            cur.close()

#============================ Update Queries ===========================

def insertCartItem(sid, pid,amount):
    with DatabaseConnection() as db:
        try:
            data = fetchCartitem(sid,pid)
            print(data)
            cur = db.cursor()
            #Check if item is already in the shopping cart
            if data is None:
                #default add an entry

                cmd = f"INSERT INTO ShoppingCartItem(ShoppingCartID,ProductID, Amount) VALUES ({sid},{pid},{amount});"
                print(cmd)
                cur.execute(cmd)
                db.commit()
            else:
                cmd = f"UPDATE ShoppingCartItem SET Amount={amount}+Amount WHERE ShoppingCartID={sid} AND ProductID={pid};"
                print(cmd)
                cur.execute(cmd)
                db.commit()
        except:
            # handle Connection errors
            print("Unexpected error:", sys.exc_info()[0])
            return 'Error'
        finally:
            cur.close()

def makeOrder(cart,items,uid):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f"INSERT INTO OrderData VALUE (DEFAULT, %s,%s,%s);"
            cur.execute(command,(uid ,datetime.now().strftime('%Y-%m-%d, %H:%M:%S'), f'Order from: {cart["ShoppingCartName"]}'))
            db.commit()
            insertOrderItems(cur.getlastrowid(),items)
        except Exception as e:
            # handle Connection errors
            print(e)
            return 'Error'
        finally:
            cur.close()

#recursive
def insertOrderItems(oid,items):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f"INSERT INTO OrderItem VALUE (%s, %s,%s,%s);"
            citem = items.pop()
            cur.execute(command,(oid, citem['ProductID'], citem['Price'], citem['Amount']))
            db.commit()
            if items != []:  # recall untill list is empty
                insertOrderItems(oid, items)
        except Exception as e:
            # handle Connection errors
            print(e)
            return 'Error'
        finally:
            cur.close()


#========================== Fetche ==========================
def fetchProductList(cond,offset):
    with DatabaseConnection() as db:
        if offset is None:
            offset = 0

        try:
            cur = db.cursor()
            if cond == "" or cond is None:
                # sql select all object, limit 10 (will be editable in future) with rarity name from rarity table and product table
#                command = "SELECT Product.ProductName,Product.Price,Rarity.RarityName,Product.ProductID FROM Product " \
#                        "INNER JOIN cykelochbad_johan.Rarity ON  Product.RarityID = Rarity.RarityID LIMIT 10 OFFSET {};".format(offset)

                command = "SELECT * FROM Product INNER JOIN Rarity ON  Product.RarityID = Rarity.RarityID LIMIT 10 OFFSET {};".format(offset)
            else:
                # sql select from every product like seach string and link with rarity table
                command = "SELECT Product.ProductName,Product.Price,Rarity.RarityName,Product.ProductID FROM " \
                        "(SELECT * FROM Product WHERE ProductName LIKE '%{}%') AS Product " \
                        "INNER JOIN cykelochbad_johan.Rarity ON  Product.RarityID = Rarity.RarityID LIMIT 10 OFFSET {};".format(cond, offset)
            cur.execute(command)
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchall()
            json_data = []
            for result in data:
                json_data.append(dict(zip(row_headers, result)))

            return json_data
        except Exception as e:
            # handle Connection errors
            return e
        finally:
            cur.close()

def getSizeOfQuerry(querry, table):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute('SELECT count(*) FROM {} where {};'.format(table,querry))
            return cur.fetchall()
        except Exception as e:
            # handle Connection or condition errors
            return e
        finally:
            cur.close()

def userExists(userID):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("SELECT EXISTS (SELECT * from Person where PersonID=%s)", (userID,))
            data = cur.fetchone()
            return data
        except Exception as e:
            return e
        finally:
            cur.close()

def fetchUser(userID):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            cur.execute("SELECT * from Person where PersonID=%s", (userID,))
            data = cur.fetchone()
            return data
        except Exception as e:
            return e
        finally:
            cur.close()
def fetchUserCart(userID):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f'SELECT * FROM ShoppingCart WHERE PersonID={userID} '
            cur.execute(command)
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchall()
            json_data = []
            for result in data:
                json_data.append(dict(zip(row_headers, result)))
            if data is None:
                return None
            else:
                return json_data
        except:
            # handle Connection errors
            return 'Error'
        finally:
            cur.close()

def fetchCartitem(sid,pid):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f'SELECT * FROM ShoppingCartItem WHERE ProductID={pid} AND ShoppingCartID={sid};'
            print(command)
            cur.execute(command)
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchone()
            if data is not None:
                json_data = []
                json_data.append(dict(zip(row_headers, data)))
                print(json_data)
                return json_data
            else:
                return None
        except:
            # handle Connection errors
            return 'Error'
        finally:
            cur.close()

def fetchCartitems(sid):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f"SELECT * FROM (SELECT * FROM ShoppingCartItem WHERE ShoppingCartID={sid}) as ShoppingCartItem " \
                      f"INNER JOIN (SELECT ProductID,ProductName,Price FROM Product) as Product " \
                      f"ON ShoppingCartItem.ProductID = Product.ProductID;"
            cur.execute(command)
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchall()
            json_data = []
            for result in data:
                json_data.append(dict(zip(row_headers, result)))

            return json_data
        except Exception as e:
            # handle Connection errors
            print(e)
            return 'Error'
        finally:
            cur.close()


def fetchCountInCart():
    #ops Prints all shoppingcarts, even between users as this is not always obvious
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f"SELECT COUNT(ShoppingCartID) as count, ShoppingCartID FROM cykelochbad_johan.ShoppingCartItem GROUP BY ShoppingCartID;"
            cur.execute(command)
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchall()
            json_data = []
            for result in data:
                json_data.append(dict(zip(row_headers, result)))
            return json_data
        except:
            # handle Connection errors
            return 'Error'
        finally:
            cur.close()
def fetchOrders(uid):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f"SELECT * FROM OrderData WHERE PersonID=%s;"
            cur.execute(command,(uid,))
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchall()
            json_data = []
            for result in data:
                json_data.append(dict(zip(row_headers, result)))
            return json_data
        except Exception as e:
            # handle Connection errors
            print(e)
            return 'Error'
        finally:
            cur.close()

def fetchOrderItems(oid):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f"SELECT * FROM OrderData WHERE OrderID=%s;"
            cur.execute(command, (oid,))
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchall()
            json_data = []
            for result in data:
                json_data.append(dict(zip(row_headers, result)))
            return json_data
        except Exception as e:
            # handle Connection errors
            print(e)
            return 'Error'
        finally:
            cur.close()

    #=============== special operations =====================
def userDebit(uid, sum):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = f"UPDATE Person SET Balance = Balance - {sum} WHERE PersonID=%s;"
            print(command)
            cur.execute(command,(uid,))
            db.commit()
        except Exception as e:
            # handle Connection errors
            print(e)
            return 'Error'
        finally:
            cur.close()



def deleteUserCart(sid):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            command = "DELETE FROM ShoppingCart WHERE ShoppingCartID =%s"
            cur.execute(command,(sid,))
            db.commit()
        except:
            # handle Connection errors
            return 'Error'
        finally:
            cur.close()


#todo fetchuserreviews
def fetchReviews(pid,offset):
    with DatabaseConnection() as db:
        try:
            cur = db.cursor()
            #
            command = f"Select * From (SELECT * FROM Review WHERE ProductID = %s) As R INNER JOIN (SELECT PersonID,FirstName, LastName From Person) AS Person ON Person.PersonID = R.PersonID  LIMIT 10 OFFSET %s;"
            cur.execute(command, (pid,offset))
            row_headers = [x[0] for x in cur.description]
            data = cur.fetchall()
            json_data = []
            for result in data:
                json_data.append(dict(zip(row_headers, result)))
            return json_data
        except Exception as e:
            # handle Connection errors
            print(e)
            return 'Error'
        finally:
            cur.close()
