from flask import render_template, session, abort, url_for, redirect, request, flash
from app import app, DatabaseConnection, login_manager
from app.wtf.signin import SignInForm
from werkzeug.security import generate_password_hash, check_password_hash
from app.objects.User import User
import flask_login
from flask_login import current_user
import json


@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if not current_user.is_authenticated:
        # Wait for sign in attempt
        form = SignInForm()
        if form.validate_on_submit():
            # Get User from database
            usr = None
            user = None
            with DatabaseConnection() as db:
                cur = db.cursor()
                cur.execute("SELECT PersonID FROM Person WHERE Email = %s", (form.email.data,))
                usr = cur.fetchone()
                if usr is not None:
                    usr = dict(zip(cur.column_names, usr))
                    user = User(id=usr['PersonID'])
                    #
            if user is not None:
                if user.check_password(form.password.data):
                    flask_login.login_user(user, remember=form.remember_me.data)
                    return redirect_dest(fallback=url_for('index'))
            else:
                abort(401)
        return render_template('wtf/signin.html', title='Sign In', form=form)
    return redirect_dest(fallback=url_for('index'))

@login_manager.user_loader
def user_loader(uid):
    return User(uid)

@login_manager.unauthorized_handler
def handle_needs_login():
    flash("You have to be logged in to access this page.", 'warning')
    return redirect(url_for('signin', next=request.endpoint))

def redirect_dest(fallback):
    dest = request.args.get('next')
    try:
        dest_url = url_for(dest)
    except:
        return redirect(fallback)
    return redirect(dest_url)
