from flask import render_template, request, redirect, url_for, session, flash, abort
from app import  app, orm
from flask_login import current_user, login_required
import json

#routed from product page
@app.route('/user/<int:uid>/<int:sid>', methods=['POST', 'GET'])
@login_required
def checkout(uid,sid):
    #valid login?
    if uid == int(current_user.id):
        itemlist = orm.fetchCartitems(sid)
        sum = 0
        for item in itemlist:
            sum += item['Price']*item['Amount']

        #when confirmation is submited start debit sequence and make orders, redirect back to userpage
        if request.method == 'POST':
            orm.userDebit(uid,sum)
            carttuple = []
            #ugly select, todo add orm entry for selecting specific cart via uid and sid
            for cart in orm.fetchUserCart(uid):
                if cart['ShoppingCartID'] == sid:
                    carttuple = cart
            #make the order
            orm.makeOrder(carttuple,orm.fetchCartitems(sid), uid)
            return redirect(url_for('userpage',uid=uid))

        return render_template('Checkout.html', shoplist=itemlist,user=orm.fetchUser(uid),sum = sum, balance=current_user.balance)
    return redirect(url_for('userpage',uid=uid))
