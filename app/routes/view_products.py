from flask import render_template
from app import DatabaseConnection
from app import app

@app.route('/view_products', methods=['GET', 'POST'])
def view_products():
    with DatabaseConnection() as db:
        cur = db.cursor()
        cur.execute('SELECT ProductID, ProductName, RarityID, Price FROM Product')
        row_headers = [x[0] for x in cur.description]
        data = cur.fetchall()
        json_data=[]
        for result in data:
            json_data.append(dict(zip(row_headers,result)))
    return render_template('view_products.html',user=user, data=json_data)