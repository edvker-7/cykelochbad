from app import app, orm
from flask import render_template, flash, session, request, abort
from flask_login import login_required, current_user
import json

@app.route('/user/<int:uid>')
@login_required
def userpage(uid):
    if uid == int(current_user.id) or current_user.roleid == 0:
        print(current_user.id)
        if orm.userExists(current_user.id) is not None:
            carts = orm.fetchUserCart(current_user.id)
            orders = orm.fetchOrders(current_user.id)
            return render_template('userpage.html',shopCart=carts,orders=orders)
        else:
            abort(404)
    abort(401)
