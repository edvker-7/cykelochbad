from app import app
from flask import session, redirect, url_for
from flask_login import login_required, logout_user


@app.route('/signout')
@login_required
def signout():
    logout_user()
    return redirect(url_for('index'))
