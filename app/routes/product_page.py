import json

from flask import render_template, session, request
from app import DatabaseConnection, app, orm
from flask_login import current_user


@app.route('/product_page/pid=<pid>', methods=['GET','POST'])
def product_page(pid):

    json_data = None
    with DatabaseConnection() as db:
        cur = db.cursor()
        cq = f"SELECT Product.ProductID, Product.ProductName,Product.Image,Product.Price,Rarity.RarityName FROM (SELECT * FROM Product WHERE ProductID = {pid}) AS Product INNER JOIN cykelochbad.Rarity ON Product.RarityID = Rarity.RarityID;"

        cur.execute(cq)
        row_headers = [x[0] for x in cur.description]
        data = cur.fetchall()
        json_data = []
        for result in data:
            json_data.append(dict(zip(row_headers, result)))

    shop = None
    count = None
    if current_user.is_authenticated:
        shop = orm.fetchUserCart(current_user.id)
        count = (orm.fetchCountInCart())
        if request.method == 'POST':
            text = request.form['textbox']
            rating = request.form['rating']
            orm.insertReview(current_user.id, pid, text, rating)

    reviews = orm.fetchReviews(pid,0)

    return render_template('product_page.html', data=json_data, shoplist = shop, shopCount=count, reviews= reviews)


