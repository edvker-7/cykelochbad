from flask import render_template, request, redirect, url_for, session, flash, abort
from app import  app, orm
from flask_login import current_user
import json

#routed from product page
@app.route('/product_page/pid=<pid>/buy', methods=['POST', 'GET'])
def buyProduct(pid):
    #Check valid user is in session todo add support for cashed shoppingcart
    if current_user.is_authenticated:
        #If User adds Additional
        if request.method == 'POST':
            #take form data
            if request.form['nameNew'] is not None:
                orm.insertShoppingCart(current_user.id,request.form['nameNew'])
        return render_template('shoppingCart.html',data=orm.fetchUserCart(current_user.id), id=pid)
    return redirect(url_for('product_page',pid=pid))

@app.route('/addShoppingcart', methods=['POST'])
def addShoppingcart():
    #Check valid user is in session todo add support for cashed shoppingcart
    if current_user.is_authenticated:
        #take form data
        if request.form['nameNew'] is not None:
            orm.insertShoppingCart(user['id'], request.form['nameNew'])
        return redirect(url_for('userpage',uid=current_user.id))
    abort(401)


@app.route('/product_page/pid=<pid>/additem', methods=['POST'])
def addToCart(pid):
    if current_user.is_authenticated:
        print(request.form)
        sid = request.form['cart']
        amount = request.form['amount']
        if sid != 'None':
            print(amount)
            orm.insertCartItem(sid,pid,amount)
        return redirect(url_for('product_page',pid=pid))
    abort(401)

@app.route('/cartitems/<int:sid>', methods=['POST', 'GET'])
def listCart(sid):
    if current_user.is_authenticated:
        if request.method == 'POST':
            # take form data
            if request.form['nameNew'] is not None:
                orm.insertShoppingCart(current_user.id, request.form['nameNew'])

        return render_template('shoppingCart.html', data=orm.fetchCartitems(sid),id=None)
    abort(401)
