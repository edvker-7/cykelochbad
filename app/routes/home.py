from app import app
from flask import render_template, session, url_for, redirect
import json

@app.route('/')
@app.route('/index')
def index():
    return redirect(url_for('search'))
