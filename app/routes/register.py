from flask import render_template, redirect, request, url_for, flash
from app import app, orm
from app.wtf.register import RegisterForm



@app.route('/register', methods=['GET', 'POST'])
def createUser():
    form = RegisterForm()
    # Wait for form submit
    if form.validate_on_submit():
        try:
            fname = form.first_name.data
            lname = form.last_name.data
            email = form.email.data
            pwd = form.password.data

            # Create an ordinary user (role 5).
            orm.insertUser(email,fname,lname,5,pwd)

            flash(f"User '{lname} {fname}' was created", 'success')
            return redirect(url_for('index'), code=303)
        except Exception as e:
            flash('An Error Occured', 'danger')
            return redirect(url_for('createUser'), code=303)
    print(form.errors)
    return render_template('wtf/register.html', title='Register', form=form)
