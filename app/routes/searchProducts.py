from flask import  render_template, request, session
from app import orm
from app import app
import json

@app.route('/search', methods=['GET', 'POST'])
def search():
    # catch Url parameters
    offset = request.args.get('page', default=0, type=int)
    searchq = request.args.get('searchkey', default="", type=str)

    # defult product fetch
    if request.method == 'POST':

        # backwards button
        try:
            # ========= fetch from database after a post =======
            searchq = request.form['searchkey']
        except:
            if "b" in request.form:
                if  offset > 10:
                    offset -= 10
            elif "n" in request.form: # forward button
                #get length of our current search
                length = orm.getSizeOfQuerry('ProductName like "%{}%"'.format(searchq), 'Product')[0][0]
                if length > offset:
                    offset = 10 + offset
        finally:
            data = orm.fetchProductList(searchq, offset)
        #print(data)
        return render_template('searchProduct.html', items=data, offset=offset)

    data = orm.fetchProductList("", offset)
    user = None
    if 'user' in session:
        user = json.loads(session['user'])
    return render_template('searchProduct.html', items=data, offset=offset, user=user)
