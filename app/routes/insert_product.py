from flask import render_template, request
from app import DatabaseConnection
from app import app, orm
from app.wtf.insert_product import InsertProductForm
from werkzeug.utils import secure_filename
from werkzeug.datastructures import CombinedMultiDict
import os

@app.route('/insert_product', methods=['GET', 'POST'])
def insert_product():
    rarities = orm.getRarities()

    form = InsertProductForm()
    form.rarity_id.choices = rarities

    if form.validate_on_submit():
        try:
            productname = form.product_name.data
            rarityid = form.rarity_id.data
            price = form.price.data
            image = form.image.data
            image_name = secure_filename(image.filename)
            pid = orm.insertProduct(productname, rarityid, image_name, price)

            image_dir = os.path.join(app.root_path, 'Images', str(pid))
            os.makedirs(image_dir, 0o755)
            image_path = os.path.join(image_dir, image_name)
            print(image_path)
            image.save(image_path)

        except Exception as e:
            print(e)
    print(form.errors)
    return render_template('wtf/insert_product.html', form=form)

