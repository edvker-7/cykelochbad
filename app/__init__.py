from flask import Flask, url_for
from flask_login import LoginManager
from app.utility.dbconnect import DatabaseConnection
import app.utility.orm as orm

app = Flask(__name__)
with app.test_request_context():
    print(url_for("static", filename="main.css"))
#app.config['STATIC_FOLDER'] = "app/static"

app.config.from_object('config.Config')
app.secret_key = app.config['SECRET_KEY']

login_manager = LoginManager(app)
login_manager.login_view = 'signin'
