from flask_login import UserMixin
from app import DatabaseConnection
from werkzeug import generate_password_hash, check_password_hash
import json

class User(UserMixin, object):
    def __init__(self, id):
        self.id = id

        with DatabaseConnection() as db:
            cur = db.cursor(dictionary=True)
            cur.execute("SELECT * FROM Person WHERE PersonID = %s", (id,))
            usr = cur.fetchone()

            self.email = usr['Email']
            self.firstname = usr['FirstName']
            self.lastname = usr['LastName']
            self.roleid = usr['RoleID']
            self.authentication_string = usr['authentication_string']
            self.balance = usr['Balance']
            self.created = str(usr['DateCreated'])

    @classmethod
    def id_exists(cls, uid):
        with DatabaseConnection() as db:
            cur = db.cursor()
            cur.execute("SELECT PersonID FROM Person WHERE PersonID = %s", (uid,))
            return not cur.fetchone() == None

    @classmethod
    def email_exists(cls, email):
        """Return User ID if user exists otherwise None"""
        with DatabaseConnection() as db:
            cur = db.cursor()
            cur.execute("SELECT PersonID FROM Person WHERE Email = %s", (email,))
            usr = cur.fetchone()
            if not usr == None:
                return usr[0]
            return None

    def set_password(self, pwd):
        self.authentication_string = generate_password_hash(pwd)

    def check_password(self, pwd):
        return check_password_hash(self.authentication_string, pwd)

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

