from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, SubmitField, IntegerField, SelectField
from wtforms import validators

class InsertProductForm(FlaskForm):
    product_name = StringField('Product name', validators=[validators.DataRequired()])
    rarity_id = SelectField('Rarity id', validators=[validators.DataRequired()], coerce=int)
    image = FileField('Image', validators=[FileRequired(), FileAllowed(['jpg', 'jpeg', 'png'], 'Images only!')])
    price = IntegerField('Price', validators=[validators.DataRequired()])
    submit = SubmitField('Submit')
