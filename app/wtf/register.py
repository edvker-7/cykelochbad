from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms import validators
from wtforms.fields.html5 import EmailField

class RegisterForm(FlaskForm):
    first_name = StringField('First name', validators=[validators.DataRequired()])
    last_name = StringField('Last name', validators=[validators.DataRequired()])
    email = EmailField('Email', validators=[validators.DataRequired(), validators.Email(granular_message=True)])
    password = PasswordField('Password', validators=[validators.DataRequired()])
    submit = SubmitField('Submit')
